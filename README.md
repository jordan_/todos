# README #

# Setup Guide #
Download [NodeJS LTS](https://nodejs.org/en/download/) 

Once installation is finished, find the source code directory and type

`npm install`

This is will install NodeJS dependencies.

# Start Server#

type: `npm start`

On browers go to `localhost:5000`

Use `Ctrl-C` to stop node server.

#Folder Structure #

`\controllers\server.js`  server side javascript   
`\model\`   -empty folder   
`\node_modules\`  node js dependencies, normally empty. gitignored this folder.    
`\public\`  
   ---css   
   ---js   
      --- main.js    client side javascript   
   ---lib  contains Dojo Toolkit librarys   
` \sql\`   sql creation script(only one table)   
 `\sqlite\`  database engine   
 `\views\`   
    ---index.html   main client side html  
`todo.db`    sqlite database   

 
#Tech Stack #
Database - Sqlite    
Backend - NodeJS  
Frontend - Dojo Toolkit   

Tested on Firefox and Chrome