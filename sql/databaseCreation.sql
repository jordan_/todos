CREATE TABLE "task" 
("id" INTEGER PRIMARY KEY  NOT NULL , 
  "text" TEXT,
  "completed_at" TEXT,
  "create_at" TEXT
)