var express = require('express');
var sqlite3 = require('sqlite3').verbose();
var bodyParser = require("body-parser");
var fs = require("fs");

//var file = "/sqlite/sqlite3";
// var db = new sqlite3.cached.Database(file, function (error) {
		// if (error) {
			// console.log("can't connect to database.");
		// }
	// });
  db = new sqlite3.Database('todo.db');
  
  // Database initialization
db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='task'",
       function(err, rows) {
  if(err !== null) {
    console.log(err);
  }
  else if(rows === undefined) {
    db.run('CREATE TABLE "task" ' +
           '("id" TEXT PRIMARY KEY NOT NULL, ' +
           '\'text\' TEXT, ' +
           '"completed_at" TEXT, ' +
		   ' "created_at"  TEXT )', function(err) {
      if(err !== null) {
        console.log(err);
      }
      else {
        console.log("SQL Table 'task' initialized.");
      }
    });
  }
  else {
    console.log("SQL Table 'task' already initialized.");
  }
});
  
  
var app = express();

//check for environment variables if deployed to cloud
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 5000
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

app.use(express.static('public'));
app.use(express.static('views'));


app.listen(server_port,server_ip_address,  function (err) {
	console.log('running server ' + server_ip_address  +' on port ' + server_port)

});

const taskSortBy = " ORDER BY created_at ASC";

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
		extended : true
	})); // support encoded bodies

//insert
app.post('/to-do/', function (req, res, next) {

	var taskObject = req.body;

	var id = taskObject.id;
	var text = taskObject.text;
    var created_at = taskObject.created_at;
	var completed_at = taskObject.completed_at;

	var sqlPreparedStatement = "INSERT INTO task(id, text, created_at, completed_at) " +
		"VALUES ($id, $text, $created_at, $completed_at)";

	db.serialize(function () {
		db.run(sqlPreparedStatement, {
			$id : id,
			$text : text,
			$created_at: created_at,
			$completed_at : completed_at
		},
			function (error) {
			if (error) {
				res.send(error);
			} else {
				res.send({'message':'Successfully inserted task into database'});
			}
		});
	});

});


//update
app.post('/to-do/:id', function (req, res, next) {

	var taskObject = req.body;

	var id = taskObject.id;
	var text = taskObject.text;
	var completed_at = taskObject.completed_at;

	var sqlPreparedStatement = 'UPDATE task ' +
		'SET  text = $text, completed_at = $completed_at ' +
		'WHERE task.id = $id ';

	db.run(sqlPreparedStatement, {
		$id : id,
		$text : text,
		$completed_at : completed_at
	},
		function (error) {
		if (error) {
			res.send(error);
		} else {
			res.send({'message':'Successfully update task into database'});
		}
	});

});

//get all
app.get('/to-do/all', function (req, res) {
	//todo limit to login in user
	var sqlStatement = "SELECT * FROM task" + taskSortBy;
	db.serialize(function () {
		db.all(sqlStatement, function (err, rows) {
			if (err) {
				res.send(err);
			} else {
				res.send(rows);
			}

		});
	});
});

// Not a requirement
// app.delete ('/deleteTask/:id', function (req, res) {
	// var taskId = req.params.id;
	// var sqlPreparedStatement = 'DELETE FROM task WHERE id = $taskId';

	// db.run(sqlPreparedStatement, {
		// $taskId : taskId,
	// },
		// function (error) {
		// if (error) {
			// res.send(error);
		// } else {
			// res.send('Successfully deleted task from the database');
		// }
	// });

// });

