require([
		'dojo/_base/declare',
		'dgrid/Grid',
		'dgrid/extensions/Pagination',
		'dstore/Memory',
		'dstore/RequestMemory',
		'dgrid/OnDemandList',
		'dgrid/Selection',
		'dojo/request/xhr'

	], function (declare, Grid, Pagination, Memory, RequestMemory, List, Selection, xhr) {

	var FILTER_TYPE = "all" //default filter;
	var ROWS_PER_PAGE = 10; //default row per page

	var addButton = document.getElementById("addTodoButton");
	addButton.addEventListener("click", function (event) {
		insertNewTask();
	}, false);

	var filteringDropdown = document.getElementById("filteringDropdown");
	filteringDropdown.addEventListener("change", function (event) {
		if (this.value === "all") {
			FILTER_TYPE = "all";
			populateGrid();
		} else if (this.value === "completed") {
			FILTER_TYPE = "completed";
			populateGrid();
		} else if (this.value === "notCompleted") {
			FILTER_TYPE = "notCompleted";
			populateGrid();
		}

	});

	var dataStore = new Memory({
			data : [],
			idProperty : 'id'
		});

	var self = this;
	
	//grid layout
	var todoColumns = [{
			field : '_checkbox',
			label : 'Checkbox',
			sortable : false,
			renderCell : function (object, value, node) {
				/*  object: each row data from api (id, text, completed_at, created_at)
				*   value: current column data (nothing in this case)
				*   node: dom node
				*/  
				var isChecked = false;
				var completed = isTaskCompleted(object);
				var checkbox = document.createElement("input");
				checkbox.setAttribute("type", "checkbox");
				if (completed) {
					checkbox.setAttribute("checked", true);
					isChecked = true;
				}

				checkbox.addEventListener("click", function (event) {
					if(isChecked){
						checkbox.removeAttribute("checked");
						isChecked = false;
						object.completed_at = "";
					}else{
						isChecked = true;
						checkbox.setAttribute("checked", true);
						var completedDate = new Date();
						object.completed_at = completedDate.toJSON();
					}
					
					
					
					//update
					updateTaskToDatabase(object);


				}, false);

				return checkbox;
			}
		}, {
			field : 'text',
			label : 'Task',
			sortable : false,
			renderCell : function (object, value, node) {

				var completed = isTaskCompleted(object);

				var divNode = document.createElement("div");
				divNode.innerHTML = value;
				if (completed) {
					divNode.className = "strikeOut";
				} else {
					divNode.className = "";
				}

				return divNode;
			}
		}, {
			field : 'created_at',
			label : 'Created Date',
			sortable : false,
			renderCell : function (object, value, node) {
				//return empty string if created_at is an empty string
				if (value) {
					var date = new Date(value);
					node.innerHTML = date.toLocaleString();
				} else {
					node.innerHTML = "";
				}
			}

		}, {
			field : 'completed_at',
			label : 'Completed Date',
			sortable : false,
			renderCell : function (object, value, node) {
				//completed date could be an empty string
				var date = new Date(value);

				if (value) { //completed_at value is '' if it's not completed
					node.innerHTML = date.toLocaleString();
				} else {
					node.innerHTML = "";
				}
			}
		}
	];
	
	//grid with pagination
	var grid = new(declare([Grid, Pagination]))({
			columns : todoColumns,
			collection : dataStore,
			pagingLinks : 1,
			firstLastArrows : true,
			noDataMessage : "Hooray! No Task!",
			rowsPerPage : ROWS_PER_PAGE
		}, 'todoGrid');
	
	//inital load of grid
	populateGrid();

	function insertNewTask() {
		//in jquery:  $('#todoTextbox')
		var textboxNode = document.getElementById("todoTextbox");
		var textValue = textboxNode.value;
		clearNodeValue(textboxNode);
		
		if(textValue.length > 0){  //don't insert empty string
		insertTaskToDatabase(textValue);
		}

	}
	function clearNodeValue(node) {
		node.value = "";
	}

	function isTaskCompleted(object) {
		var completedDate = new Date(object.completed_at);
		var createdDate = new Date(object.created_at);

		if (object.completed_at) { //object.completed_at is '' if it's not completed
			return completedDate >= createdDate;
		} else {
			return false;
		}
	}
	
	var self = this;
	function populateGrid() {
		
		//ajax call,  similar to jquery: $.ajax({})
		xhr.get("/to-do/all", {
			handleAs : "json"
		}).then(function (data) {
	
			if (FILTER_TYPE === 'completed') {
				//use array method to filter
				data = data.filter(function (result) {
						var completed_at = result.completed_at;
						var completedDate = new Date(completed_at);
						var createdDate = new Date(result.created_at);
						return completed_at && (completedDate > createdDate);  //completed_at could be an empty string
					});

			} else if (FILTER_TYPE === 'notCompleted') { //not completed
				//user array method to filter
				data = data.filter(function (result) {
						var completed_at = result.completed_at;
						var completedDate = new Date(completed_at);
						var createdDate = new Date(result.created_at); 
						return !completed_at || (completedDate < createdDate); //completed_at could be an empty string
					});
			}

			if (data.length > 0) {
				var dataStore = new Memory({
						data : data,
						idProperty : 'id'
					});
				
				//refresh with new data 
				var currentPage = grid._currentPage;
				grid.set("collection", dataStore);
				//keep current page only if current page could fit all data
				if ((currentPage *  ROWS_PER_PAGE - data.length ) <= ROWS_PER_PAGE) {
					grid.gotoPage(currentPage);
				}
				


			}

			//grid.refresh();
		}, function (err) {
			// Handle the error condition
			console.log("/to-do/all error");
		});

	}

	function insertTaskToDatabase(textValue) {

		var createdDate = new Date();
		var newTaskObject = {};
		newTaskObject.id = guid();
		newTaskObject.text = textValue;
		newTaskObject.created_at = createdDate.toJSON();
		newTaskObject.completed_at = "";
		
		//in jquery: $.ajax()
		xhr.post("/to-do/", {
			handleAs : "json",
			data : newTaskObject,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).then(function (data) {
			populateGrid();
		}, function (err) {
			// Handle the error condition
			console.log(err);
			console.log("insert task error");
		});
	}

	function updateTaskToDatabase(object) {
		var id = object.id;
		//in jquery: $.ajax()
		xhr.post("/to-do/id=" + id, {
			handleAs : "json",
			data : object,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).then(function (data) {
			populateGrid();
		}, function (err) {
			// Handle the error condition
			console.log("update task error");
		});
	}

	//http://stackoverflow.com/a/105074
	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
	}
});
